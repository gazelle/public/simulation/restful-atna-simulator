package net.ihe.gazelle.restfulatna.mustache;

import org.hl7.fhir.r4.model.AuditEvent;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Map;

public interface IMapToMustache {

    Map<String, String> mapReqAuditEventToMustache(AuditEvent auditEvent);

    default String dateNow() {
        return ZonedDateTime.now(ZoneId.of("Europe/Zurich")).truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + "Z";
    }

}
