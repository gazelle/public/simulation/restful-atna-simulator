package net.ihe.gazelle.restfulatna.mustache;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

public class MustacheTemplating {


    public Mustache generateBundleResponseFromJson() {
        MustacheFactory mf = new DefaultMustacheFactory();
        return mf.compile("templates/mustache/bundle_batch_response_example.json.mustache");
    }
    public Mustache generateOperationOutcomeResponseFromJson() {
        MustacheFactory mf = new DefaultMustacheFactory();
        return mf.compile("templates/mustache/operation_outcome_example.json.mustache");
    }

    public Mustache generateResourceResponseFromJson() {
        MustacheFactory mf = new DefaultMustacheFactory();
        return mf.compile("templates/mustache/resource_response_example.json.mustache");
    }

}
