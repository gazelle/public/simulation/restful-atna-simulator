package net.ihe.gazelle.restfulatna.model;

public final class ConstantValues {
    /**
     * Add private constructor to prevent implicit one.
     */
    private ConstantValues() {
    }

    public static final String AUDIT_EVENT = "AuditEvent";
    public static final String AUDIT_EVENT_ID = "auditEvent.id";
    public static final String DATE = "date";
    public static final String ID = "id";
    public static final String ISSUE_CODE = "issue.code";
    public static final String ISSUE_DETAILS_CODE = "issue.details.code";
    public static final String ISSUE_SEVERITY = "issue.severity";
    public static final String POST = "POST";


}
