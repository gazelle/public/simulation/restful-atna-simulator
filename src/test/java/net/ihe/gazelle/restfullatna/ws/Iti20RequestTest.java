package net.ihe.gazelle.restfullatna.ws;

import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import net.ihe.gazelle.restfulatna.business.ResponseManager;
import net.ihe.gazelle.restfulatna.model.ConstantValues;
import net.ihe.gazelle.restfulatna.model.ErrorMessages;
import net.ihe.gazelle.restfulatna.ws.provider.AuditEventBundleProvider;
import net.ihe.gazelle.restfulatna.ws.provider.AuditEventResourceProvider;
import net.ihe.gazelle.restfullatna.mock.PostRequestMock;
import org.hl7.fhir.r4.model.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;

class Iti20RequestTest {

    private static ResponseManager responseManager;
    private static AuditEventBundleProvider auditEventBundleProvider;
    private static AuditEventResourceProvider auditEventResourceProvider;
    private static Bundle iti20BundleAuditEvent;
    private static Bundle iti20BundlePatient;
    private static Bundle iti20BundleAuditEventXml;
    private static Bundle iti20BundleHttpPUT;
    private static Bundle iti20BundleNoEntries;
    private static Bundle iti20BundleNoResource;
    private static Bundle iti20BundleNoType;

    private static AuditEvent iti20ResourceAuditEvent;
    private static AuditEvent iti20ResourceAuditEventEmpty;

    private static AuditEvent iti20ResourceAuditEventXml;

    @BeforeAll
    static void setup() throws IOException {
        auditEventBundleProvider = new AuditEventBundleProvider();
        auditEventResourceProvider = new AuditEventResourceProvider();
        responseManager = new ResponseManager();

        iti20BundleAuditEvent = PostRequestMock.getBundleAuditEvent();
        iti20BundleAuditEventXml = PostRequestMock.getBundleAuditEventXml();
        iti20BundlePatient = PostRequestMock.getBundlePatient();
        iti20BundleHttpPUT = PostRequestMock.getBundleWithHttpPut();
        iti20BundleNoEntries = PostRequestMock.getBundleWithNoEntries();
        iti20BundleNoResource = PostRequestMock.getBundleWithNoResource();
        iti20BundleNoType = PostRequestMock.getBundleWithNoType();

        iti20ResourceAuditEvent = PostRequestMock.getResourceAuditEvent();
        iti20ResourceAuditEventEmpty = PostRequestMock.getResourceAuditEventEmpty();
        iti20ResourceAuditEventXml = PostRequestMock.getResourceAuditEventXml();
    }

    @Test
    void createResponseJSONRequestPositiveCase() {
        MethodOutcome mo = auditEventBundleProvider.create(iti20BundleAuditEvent);
        Bundle bundleResponse = (Bundle) mo.getResource();
        Bundle.BundleEntryResponseComponent responseFromBundle = bundleResponse.getEntry().get(1).getResponse();
        String status = responseFromBundle.getStatus();
        String outComeFromResponseId = responseFromBundle.getOutcome().getId();
        String operationOutComeId = mo.getOperationOutcome().getIdElement().getIdPart();
        Assertions.assertEquals("200 OK", status);
        Assertions.assertEquals("OperationOutcome/"+operationOutComeId,outComeFromResponseId);
    }

    @Test
    void createResponseXMLRequestPositiveCase() {
        MethodOutcome mo = auditEventBundleProvider.create(iti20BundleAuditEventXml);
        Bundle bundleResponse = (Bundle) mo.getResource();
        String status = bundleResponse.getEntry().get(1).getResponse().getStatus();
        String operationOutComeId = mo.getOperationOutcome().getIdElement().getIdPart();
        Assertions.assertEquals("200 OK", status);
        Assertions.assertEquals("7357",operationOutComeId);
    }

    @Test
    void createResponseWithNullBundleNegativeTest() {
        InvalidRequestException ire = Assertions.assertThrows(InvalidRequestException.class, () -> responseManager.createBundleReponse(null));
        Assertions.assertEquals(ErrorMessages.BUNDLE_NULL_EMPTY, ire.getMessage());
    }

    @Test
    void createResponseWithEmptyBundleNegativeCase() {
        Bundle iti20BundleEmpty = new Bundle();
        InvalidRequestException ire = Assertions.assertThrows(InvalidRequestException.class, () -> responseManager.createBundleReponse(iti20BundleEmpty));
        Assertions.assertEquals(ErrorMessages.BUNDLE_NULL_EMPTY, ire.getMessage());
    }

    @Test
    void createResponseWithEmptyListOfEntriesNegativeCase() {
        InvalidRequestException ire = Assertions.assertThrows(InvalidRequestException.class, () -> responseManager.createBundleReponse(iti20BundleNoEntries));
        Assertions.assertEquals(ErrorMessages.LIST_ENTRIES_EMPTY_NULL, ire.getMessage());
    }

    @Test
    void createResponseWithNullListOfEntriesNegativeCase() {
        Bundle iti20NullEntriesBundle = iti20BundleNoEntries.setEntry(null);
        InvalidRequestException ire = Assertions.assertThrows(InvalidRequestException.class, () -> responseManager.createBundleReponse(iti20NullEntriesBundle));
        Assertions.assertEquals(ErrorMessages.LIST_ENTRIES_EMPTY_NULL, ire.getMessage());
    }

    @Test
    void createResponseWithNoResourceNegativeCase() {
        InvalidRequestException ire = Assertions.assertThrows(InvalidRequestException.class, () -> responseManager.createBundleReponse(iti20BundleNoResource));
        Assertions.assertEquals(ErrorMessages.AUDIT_EVENT_WITH_POST, ire.getMessage());
    }

    @Test
    void createResponseWithPatientNegativeCase() {
        InvalidRequestException ire = Assertions.assertThrows(InvalidRequestException.class, () -> responseManager.createBundleReponse(iti20BundlePatient));
        Assertions.assertEquals(ErrorMessages.AUDIT_EVENT_WITH_POST, ire.getMessage());
    }

    @Test
    void createResponseWithBatchTypeNegativeCase() {
        Bundle iti20BundleDocument = new Bundle().setType(Bundle.BundleType.DOCUMENT);
        InvalidRequestException ire = Assertions.assertThrows(InvalidRequestException.class, () -> responseManager.createBundleReponse(iti20BundleDocument));
        Assertions.assertEquals(ErrorMessages.BUNDLE_TYPE_ERROR, ire.getMessage());
    }

    @Test
    void createResponseWithNullTypeNegativeCase() {
        InvalidRequestException ire = Assertions.assertThrows(InvalidRequestException.class, () -> responseManager.createBundleReponse(iti20BundleNoType));
        Assertions.assertEquals(ErrorMessages.BUNDLE_TYPE_NULL, ire.getMessage());
    }

    @Test
    void createResponseWitHttpPutNegativeCase() {
        InvalidRequestException ire = Assertions.assertThrows(InvalidRequestException.class, () -> responseManager.createBundleReponse(iti20BundleHttpPUT));
        Assertions.assertEquals(ErrorMessages.AUDIT_EVENT_WITH_POST, ire.getMessage());
    }

    /**
     * Tests for AuditEvent Resource transaction
     */

    @Test
    void createResponseWithAuditEventJSONRequestPositiveCase() {
        MethodOutcome mo = auditEventResourceProvider.create(iti20ResourceAuditEvent);
        AuditEvent bundleResponse = (AuditEvent) mo.getResource();
        String resourceType = bundleResponse.getResourceType().name();
        Assertions.assertEquals(ConstantValues.AUDIT_EVENT, resourceType);
    }

    @Test
    void createResponseWithAuditEventXMLRequestPositiveCase() {
        MethodOutcome mo = auditEventResourceProvider.create(iti20ResourceAuditEventXml);
        AuditEvent bundleResponse = (AuditEvent) mo.getResource();
        String resourceType = bundleResponse.getResourceType().name();
        Assertions.assertEquals(ConstantValues.AUDIT_EVENT, resourceType);
    }

    @Test
    void createResponseWithEmptyAuditEventRequestNegativeCase() {
        Assertions.assertThrows(InvalidRequestException.class, () -> auditEventResourceProvider.create(iti20ResourceAuditEventEmpty));
    }

}
